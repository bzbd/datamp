# ORM 类 --》表
# 类对象 --》表中记录
from datetime import datetime
from ext import db
import snowflake.client


# api调用
class BaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)


class DataStandard(BaseModel):
    __tablename__ = 'data_standard'

    standard_name = db.Column(db.String(50))
    standard_content = db.Column(db.String(200))
    date_time = db.Column(db.DateTime, default=datetime.now)
    standard_style = db.Column(db.String(50))
    standard_publisher = db.Column(db.String(50))
    update_time = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
    standard_introduction = db.Column(db.String(200))

    def __str__(self):
        return "Standard: %s %s %s %s %s" % (
            self.standard_name, self.standard_style, self.standard_publisher, self.update_time,
            self.standard_introduction)
