# ORM 类 --》表
# 类对象 --》表中记录
from datetime import datetime
from ext import db
from datamp.data.models import *
from sqlalchemy import text, UniqueConstraint
import snowflake.client

# api调用
class BaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)



class RevokeTokenModel(BaseModel):
    """Revoked Token Model"""
    __tablename__ = 'revoke_token_model'
    id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
    # __table_args__ = {'extend_existing': True}
    jti = db.Column(db.String(120))

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)



# 001 --表汇总--#20211024
class DataObject(BaseModel):
    __tablename__ = 'data_object'
    object_name = db.Column(db.String(50), unique=True)
    object_table_name = db.Column(db.String(50), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 005--用户角色--#20211016#20211024
class UserRole(BaseModel):
    __tablename__ = 'user_role'
    role_name = db.Column(db.String(50), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 006 --系统功能--#20211024
class SystemFunction(BaseModel):
    __tablename__ = 'system_function'
    __table_args__ = (
        UniqueConstraint(
            'function_name',
            'decomposition_method',
            name='function_name_with_decomposition_method'),)
    function_name = db.Column(db.String(50))
    decomposition_method = db.Column(db.BIGINT, db.ForeignKey('decomposition_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 008 --用户角色to系统功能--多对多--#20211024
class UserRoleSystemFunctionAccess(BaseModel):
    __tablename__ = 'user_role_system_function_access'
    __table_args__ = (
        UniqueConstraint(
            'user_role',
            'system_function',
            name='user_role_with_system_function'),)
    user_role = db.Column(db.BIGINT, db.ForeignKey('user_role.id'))
    system_function = db.Column(db.BIGINT, db.ForeignKey('system_function.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 009--系统功能模块--#20211024
class SystemFunctionModule(BaseModel):
    __tablename__ = 'system_function_module'
    __table_args__ = (
        UniqueConstraint(
            'module_name',
            'decomposition_method',
            name='module_name_with_decomposition_method'),)
    module_name = db.Column(db.String(50))
    decomposition_method = db.Column(db.BIGINT, db.ForeignKey('decomposition_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))

    data_unit = db.relationship("DataUnit", backref="system_function_module",secondary="system_function_module_data_unit")





# 010 --系统功能模块分类--多对多--#20211024
class SystemFunctionModuleClassification(BaseModel):
    __tablename__ = 'system_function_module_classification'
    __table_args__ = (
        UniqueConstraint(
            'system_function_module',
            'system_function',
            name='system_function_module_with_system_function'),)
    system_function_module = db.Column(db.BIGINT, db.ForeignKey('system_function_module.id'))
    system_function = db.Column(db.BIGINT, db.ForeignKey('system_function.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 011--数据单元--#20211024
class DataUnit(BaseModel):
    __tablename__ = 'data_unit'
    unit_name = db.Column(db.String(50), unique=True)
    data_unit_URL = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 012--系统功能模块to数据单元--多对多--#20211024
class SystemFunctionModuleDataUnit(BaseModel):
    __tablename__ = 'system_function_module_data_unit'
    __table_args__ = (
        UniqueConstraint(
            'data_unit',
            'system_function_module',
            name='data_unit_with_system_function_module'),)
    data_unit = db.Column(db.BIGINT, db.ForeignKey('data_unit.id'))
    system_function_module = db.Column(db.BIGINT, db.ForeignKey('system_function_module.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 013 --学段--#20211024
class LearningStage(BaseModel):
    __tablename__ = 'learning_stage'
    stage_name = db.Column(db.String(50), unique=True)
    stage_number = db.Column(db.BIGINT, unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 014--学级--#20211024
class learning_grade(BaseModel):
    __tablename__ = 'learning_grade'
    grade_name = db.Column(db.String(50), unique=True)
    grade_number = db.Column(db.BIGINT, unique=True)
    learning_stage = db.Column(db.BIGINT, db.ForeignKey('learning_stage.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 015 --学期--#20211024
class LearningSemester(BaseModel):
    __tablename__ = 'learning_semester'
    semester_name = db.Column(db.String(50), unique=True)
    semester_number = db.Column(db.BIGINT, unique=True)
    learning_stage = db.Column(db.BIGINT, db.ForeignKey('learning_stage.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 016--独立实体类型--#20211025
class SeparateEntityType(BaseModel):
    __tablename__ = 'separate_entity_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(50))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 017 --独立实体--#20211025
class SeparateEntity(BaseModel):
    __tablename__ = 'separate_entity'
    entity_name = db.Column(db.String(50))
    unique_code = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 018--独立实体to分类--多对多--#20211025
class SeparateEntityClassification(BaseModel):
    __tablename__ = 'separate_entity_classification'
    __table_args__ = (
        UniqueConstraint(
            'separate_entity',
            'separate_entity_type',
            name='separate_entity_with_separate_entity_type'),)
    separate_entity = db.Column(db.BIGINT, db.ForeignKey('separate_entity.id'))
    separate_entity_type = db.Column(db.BIGINT, db.ForeignKey('separate_entity_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 019--学校类型--#20211025
class SchoolType(BaseModel):
    __tablename__ = 'school_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(50))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 020--学校--#20211025
class School(BaseModel):
    __tablename__ = 'school'
    school = db.Column(db.BIGINT, db.ForeignKey('separate_entity.id'))
    unique_number = db.Column(db.String(100), unique=True)
    province = db.Column(db.String(50))
    city = db.Column(db.String(50))
    district = db.Column(db.String(50))
    street = db.Column(db.String(50))
    building_number = db.Column(db.String(50))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 021--学校to学段--多对多--#20211025
class SchoolLearningStage(BaseModel):
    __tablename__ = 'school_learning_stage'
    __table_args__ = (
        UniqueConstraint(
            'school',
            'learning_stage',
            name='school_with_learning_stage'),)
    school = db.Column(db.BIGINT, db.ForeignKey('school.id'))
    learning_stage = db.Column(db.BIGINT, db.ForeignKey('learning_stage.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 022--学校to分类--多对多--#20211025
class SchoolClassification(BaseModel):
    __tablename__ = 'school_classification'
    __table_args__ = (
        UniqueConstraint(
            'school',
            'school_type',
            name='school_with_school_type'),)
    school = db.Column(db.BIGINT, db.ForeignKey('school.id'))
    school_type = db.Column(db.BIGINT, db.ForeignKey('school_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 023--班级--#20211025
class SingleClass(BaseModel):
    __tablename__ = 'single_class'
    __table_args__ = (
        UniqueConstraint(
            'class_number',
            'learning_grade',
            'school',
            name='class_number_with_learning_grade_with_school'),)
    class_name = db.Column(db.String(100), unique=True)
    class_number = db.Column(db.BIGINT)
    learning_grade = db.Column(db.BIGINT, db.ForeignKey('learning_grade.id'))
    school = db.Column(db.BIGINT, db.ForeignKey('school.id'))
    group = db.Column(db.BIGINT, db.ForeignKey('group.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))





# 024--班级类型--#20211025
class SingleClassType(BaseModel):
    __tablename__ = 'single_class_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(50))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 025--班级to分类--多对多--#20211025
class SingleClassClassification(BaseModel):
    __tablename__ = 'single_class_classification'
    __table_args__ = (
        UniqueConstraint(
            'single_class',
            'single_class_type',
            name='single_class_with_single_class_type'),)
    single_class = db.Column(db.BIGINT, db.ForeignKey('single_class.id'))
    single_class_type = db.Column(db.BIGINT, db.ForeignKey('single_class_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 026--学生--#20211025
class Student(BaseModel):
    __tablename__ = 'student'
    unique_number = db.Column(db.String(100), unique=True)
    real_name = db.Column(db.String(100))
    birth_date = db.Column(db.DateTime)
    gender = db.Column(db.String(20))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


    student_type = db.relationship("StudentType", backref="student",secondary="student_classification")
    group = db.relationship("Group", backref="student",secondary="group_student")





# 027--学生类型--#20211025
class StudentType(BaseModel):
    __tablename__ = 'student_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(50))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))





# 028--学生to分类--多对多--#20211025
class StudentClassification(BaseModel):
    __tablename__ = 'student_classification'
    __table_args__ = (
        UniqueConstraint(
            'student',
            'student_type',
            name='student_with_student_type'),)
    student = db.Column(db.BIGINT, db.ForeignKey('student.id'))
    student_type = db.Column(db.BIGINT, db.ForeignKey('student_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 029--班级to学生--多对多--#20211025
class SingleClassStudent(BaseModel):
    __tablename__ = 'single_class_student'
    __table_args__ = (
        UniqueConstraint(
            'student',
            'single_class',
            name='student_with_single_class'),)
    student = db.Column(db.BIGINT, db.ForeignKey('student.id'))
    single_class = db.Column(db.BIGINT, db.ForeignKey('single_class.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 030--组类型--#20211025
class GroupType(BaseModel):
    __tablename__ = 'group_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(50))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 031--组--#20211025
class Group(BaseModel):
    __tablename__ = 'group'
    group_name = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 032--组to分类--多对多--#20211025
class GroupClassification(BaseModel):
    __tablename__ = 'group_classification'
    __table_args__ = (
        UniqueConstraint(
            'group',
            'group_type',
            name='group_with_group_type'),)
    group = db.Column(db.BIGINT, db.ForeignKey('group.id'))
    group_type = db.Column(db.BIGINT, db.ForeignKey('group_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 033--组to学生--多对多--#20211025
class GroupStudent(BaseModel):
    __tablename__ = 'group_student'
    __table_args__ = (
        UniqueConstraint(
            'student',
            'group',
            name='student_with_group'),)
    student = db.Column(db.BIGINT, db.ForeignKey('student.id'))
    group = db.Column(db.BIGINT, db.ForeignKey('group.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 034--注册用户--#20211025
class RegisteredUser(BaseModel):
    __tablename__ = 'registered_user'
    user_name = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(300))
    real_name = db.Column(db.String(100))
    email_address = db.Column(db.String(100), unique=True)
    mobile_number = db.Column(db.String(100), unique=True)
    user_role = db.Column(db.BIGINT, db.ForeignKey('user_role.id'))
    organization = db.Column(db.BIGINT, db.ForeignKey('separate_entity.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 035--注册教育用户--#20211025
class RegisteredEducationalUser(BaseModel):
    __tablename__ = 'registered_educational_user'
    registered_educational_user = db.Column(db.BIGINT, db.ForeignKey('registered_user.id'))
    unique_number = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 036 --学科--#20211025
class Subject(BaseModel):
    __tablename__ = 'subject'
    subject_name = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 037 --学科类型--#20211025
class SubjectType(BaseModel):
    __tablename__ = 'subject_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(100))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 038 --学科to分类--多对多--#20211025
class SubjectClassification(BaseModel):
    __tablename__ = 'subject_classification'
    __table_args__ = (
        UniqueConstraint(
            'subject',
            'subject_type',
            name='subject_with_subject_type'),)
    subject = db.Column(db.BIGINT, db.ForeignKey('subject.id'))
    subject_type = db.Column(db.BIGINT, db.ForeignKey('subject_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 039 --学科to学段--多对多--#20211025
class StageSubject(BaseModel):
    __tablename__ = 'stage_subject'
    __table_args__ = (
        UniqueConstraint(
            'subject',
            'learning_stage',
            name='subject_with_learning_stage'),)
    subject_name = db.Column(db.String(100), unique=True)
    subject = db.Column(db.BIGINT, db.ForeignKey('subject.id'))
    learning_stage = db.Column(db.BIGINT, db.ForeignKey('learning_stage.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 040 --学段学科类型--#20211025
class StageSubjectType(BaseModel):
    __tablename__ = 'stage_subject_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(100))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 041--学段学科分类--多对多--#20211025
class StageSubjectClassification(BaseModel):
    __tablename__ = 'stage_subject_classification'
    __table_args__ = (
        UniqueConstraint(
            'stage_subject',
            'stage_subject_type',
            name='stage_subject_with_stage_subject_type'),)
    stage_subject = db.Column(db.BIGINT, db.ForeignKey('stage_subject.id'))
    stage_subject_type = db.Column(db.BIGINT, db.ForeignKey('stage_subject_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 042 --学期课程类型--#20211025
class SemesterCourseType(BaseModel):
    __tablename__ = 'semester_course_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(100))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 043 --学期课程--多对多--#20211025
class SemesterCourse(BaseModel):
    __tablename__ = 'semester_course'
    course_name = db.Column(db.String(100), unique=True)
    subject = db.Column(db.BIGINT, db.ForeignKey('subject.id'))
    learning_semester = db.Column(db.BIGINT, db.ForeignKey('learning_semester.id'))
    course_owner = db.Column(db.BIGINT, db.ForeignKey('separate_entity.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 044--学期课程分类--多对多--#20211025
class SemesterCourseClassification(BaseModel):
    __tablename__ = 'semester_course_classification'
    __table_args__ = (
        UniqueConstraint(
            'semester_course',
            'semester_course_type',
            name='semester_course_with_semester_course_type'),)
    semester_course = db.Column(db.BIGINT, db.ForeignKey('semester_course.id'))
    semester_course_type = db.Column(db.BIGINT, db.ForeignKey('semester_course_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 045 --学期课程关系--多对多--#20211025
class SemesterCoursesRelation(BaseModel):
    __tablename__ = 'semester_courses_relation'
    __table_args__ = (
        UniqueConstraint(
            'one_semester_course',
            'another_semester_course',
            name='one_semester_course_with_another_semester_course'),)
    one_semester_course = db.Column(db.BIGINT, db.ForeignKey('semester_course.id'))
    another_semester_course = db.Column(db.BIGINT, db.ForeignKey('semester_course.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 048 --教师--#20211025
class Teacher(BaseModel):
    __tablename__ = 'teacher'
    unique_number = db.Column(db.String(100), unique=True)
    real_name = db.Column(db.String(100))
    ID_number = db.Column(db.String(100), unique=True)
    birth_date = db.Column(db.DateTime)
    gender = db.Column(db.String(20))
    school = db.Column(db.BIGINT, db.ForeignKey('school.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 049--学段学科相关教师--#20211025
class StageSubjectRelatedTeacher(BaseModel):
    __tablename__ = 'stage_subject_related_teacher'
    __table_args__ = (
        UniqueConstraint(
            'teacher',
            'stage_subject',
            name='teacher_with_stage_subject'),)
    teacher = db.Column(db.BIGINT, db.ForeignKey('teacher.id'))
    stage_subject = db.Column(db.BIGINT, db.ForeignKey('stage_subject.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 050--教师学期课程--#20211025
class TeacherTeachingSemesterCourse(BaseModel):
    __tablename__ = 'teacher_teaching_semester_course'
    __table_args__ = (
        UniqueConstraint(
            'teacher',
            'semester_course',
            name='teacher_with_semester_course'),)
    teacher = db.Column(db.BIGINT, db.ForeignKey('teacher.id'))
    semester_course = db.Column(db.BIGINT, db.ForeignKey('semester_course.id'))
    teaching_method = db.Column(db.BIGINT, db.ForeignKey('teaching_method.id'))
    learning_method = db.Column(db.BIGINT, db.ForeignKey('learning_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 051--班级学期课程--#20211025
class SingleClassSemesterCourseTeaching(BaseModel):
    __tablename__ = 'single_class_semester_course_teaching'
    __table_args__ = (
        UniqueConstraint(
            'single_class',
            'semester_course_teaching',
            name='single_class_with_semester_course_teaching'),)
    single_class = db.Column(db.BIGINT, db.ForeignKey('single_class.id'))
    semester_course_teaching = db.Column(db.BIGINT, db.ForeignKey('teacher_teaching_semester_course.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))

