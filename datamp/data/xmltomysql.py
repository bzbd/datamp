from flask import Flask
from flask_sqlalchemy import SQLAlchemy


# coding:utf-8
import xml
import xml.etree.ElementTree as ET

"""
实现从xml文件中读取数据
"""
# 全局唯一标识
unique_id = 1


# 遍历所有的节点
def walkData(root_node, level, result_list):
    global unique_id
    temp_list = root_node.tag
    result_list.append(temp_list)
    unique_id += 1

    # 遍历每个子节点
    children_node = root_node.getchildren()
    if len(children_node) == 0:
        return
    for child in children_node:
        walkData(child, level + 1, result_list)
    return


def getXmlData(file_name):
    level = 1  # 节点的深度从1开始
    result_list = []
    root = ET.parse(file_name).getroot()
    walkData(root, level, result_list)

    return result_list

file_name = 'test0801.xml'
R = getXmlData(file_name)
print('R:',R)
print('R:',type(R))
for x in R:
    print(x)



app=Flask(__name__)

# 连接数据库
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:12345678@127.0.0.1:3306/flask07'
# 设置是否跟踪数据库的修改情况，一般不跟踪
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# 数据库操作时是否显示原始SQL语句，一般都是打开的，因为我们后台要日志
app.config['SQLALCHEMY_ECHO'] = True

# 实例化orm框架的操作对象，后续数据库操作，都要基于操作对象来完成
db = SQLAlchemy(app)

# 声明模型类
class Role(db.Model):
    __tablename__ = R[0]    #设置表名
    id = db.Column(db.INTEGER,primary_key=True)    #设置字段，以及属性
    R[1] = db.Column(db.String(10))







