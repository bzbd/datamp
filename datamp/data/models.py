# ORM 类 --》表
# 类对象 --》表中记录
from datetime import datetime

from flask import jsonify
from sqlalchemy import UniqueConstraint

from ext import db
import snowflake.client
from sqlalchemy import text, UniqueConstraint


# api调用
class BaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)


# test --指标--#20211028
class Indicator(BaseModel):
    __tablename__ = 'indicator'
    school = db.Column(db.String(100))
    classname=db.Column(db.String(100))
    unique_number = db.Column(db.String(100))
    student_name= db.Column(db.String(100))
    indicator_name=db.Column(db.String(100))
    indicator_value = db.Column(db.String(100))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))

    def to_dict(self):
        return {attr: getattr(self, attr) for attr in self.__mapper__.attrs.keys()}


# test --XAPI--#20211028
class XAPItest(BaseModel):
    __tablename__ = 'xapi_test'
    school = db.Column(db.String(100))
    classname = db.Column(db.String(100))
    unique_number = db.Column(db.String(100))
    student_name = db.Column(db.String(100))
    xapi_name = db.Column(db.String(100))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))




# 002 --分的方法--#20211024
class PartitioningMethod(BaseModel):
    __tablename__ = 'partitioning_method'
    method_name = db.Column(db.String(50), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 003 --分解方法--#20211024
class DecompositionMethod(BaseModel):
    __tablename__ = 'decomposition_method'
    decomposition_method = db.Column(db.BIGINT, db.ForeignKey('partitioning_method.id'))
    dimension = db.Column(db.String(50))
    object = db.Column(db.String(50))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 004 --分解方法--#20211024
class ClassificationMethod(BaseModel):
    __tablename__ = 'classification_method'
    classification_method = db.Column(db.BIGINT, db.ForeignKey('partitioning_method.id'))
    dimension = db.Column(db.String(50))
    object = db.Column(db.String(50))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 007 --常用字符--#20211024
class CustomCharacter(BaseModel):
    __tablename__ = 'custom_character'
    character = db.Column(db.String(50), unique=True)
    text_format = db.Column(db.String(50), unique=True)
    meaning = db.Column(db.String(50), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 046 --教学方法--#20211025
class TeachingMethod(BaseModel):
    __tablename__ = 'teaching_method'
    __table_args__ = (
        UniqueConstraint(
            'method_name',
            'classification_method',
            name='method_name_with_classification_method'),)
    method_name = db.Column(db.String(100))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 047 --学习方法--#20211025
class LearningMethod(BaseModel):
    __tablename__ = 'learning_method'
    __table_args__ = (
        UniqueConstraint(
            'method_name',
            'classification_method',
            name='method_name_with_classification_method'),)
    method_name = db.Column(db.String(100))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 052--概念类型--#20211025
class ConceptType(BaseModel):
    __tablename__ = 'concept_type'
    __table_args__ = (
        UniqueConstraint(
            'type_name',
            'classification_method',
            name='type_name_with_classification_method'),)
    type_name = db.Column(db.String(100))
    classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 053--概念--#20211025
class Concept(BaseModel):
    __tablename__ = 'concept'
    concept_text = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 054--概念分类--#20211025
class ConceptClassification(BaseModel):
    __tablename__ = 'concept_classification'
    __table_args__ = (
        UniqueConstraint(
            'concept',
            'concept_type',
            name='concept_with_concept_type'),)
    concept = db.Column(db.BIGINT, db.ForeignKey('concept.id'))
    concept_type = db.Column(db.BIGINT, db.ForeignKey('concept_type.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 055--概念关系--#20211025
class ConceptRelation(BaseModel):
    __tablename__ = 'concept_relation'
    __table_args__ = (
        UniqueConstraint(
            'one_concept',
            'another_concept',
            name='one_concept_with_another_concept'),)
    one_concept = db.Column(db.BIGINT, db.ForeignKey('concept_classification.id'))
    another_concept = db.Column(db.BIGINT, db.ForeignKey('concept_classification.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 056--学习因素节点--#20211025
class LearningFactorNode(BaseModel):
    __tablename__ = 'learning_factor_node'
    node_full_name = db.Column(db.String(100), unique=True)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


# 057--学习实体--#20211025
class LearningEntity(BaseModel):
    __tablename__ = 'learning_entity'
    __table_args__ = (
        UniqueConstraint(
            'entity_name',
            'decomposition_method',
            'upper_level_generic_entity',
            name='entity_with_decomposition_with_upper_entity'),)
    learning_entity = db.Column(db.BIGINT, db.ForeignKey('learning_factor_node.id'), unique=True)
    entity_name = db.Column(db.String(100))
    decomposition_method = db.Column(db.BIGINT, db.ForeignKey('decomposition_method.id'))
    upper_level_generic_entity = db.Column(db.BIGINT, db.ForeignKey('learning_entity.id'))
    uppermost_level_generic_entity = db.Column(db.BIGINT, db.ForeignKey('learning_entity.id'))
    entity_level = db.Column(db.BIGINT)
    starting_ability = db.Column(db.Boolean, server_default=text('False'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


    learning_factor_node = db.relationship("LearningFactorNode", backref="learning_entity",secondary="learning_entitys_activity")





# 058--学习实体行为--#20211025
class LearningEntityBehavior(BaseModel):
    __tablename__ = 'learning_entity_behavior'
    __table_args__ = (
        UniqueConstraint(
            'learning_entity',
            'behavior_name',
            'decomposition_method',
            'upper_level_generic_behavior',
            name='entity_with_behavior_with_decomposition_with_upper_behavior'),)
    learning_entity_behavior = db.Column(db.BIGINT, db.ForeignKey('learning_factor_node.id'), unique=True)
    learning_entity = db.Column(db.BIGINT, db.ForeignKey('learning_entity.id'))
    behavior_name = db.Column(db.String(100))
    decomposition_method = db.Column(db.BIGINT, db.ForeignKey('decomposition_method.id'))
    upper_level_generic_behavior = db.Column(db.BIGINT, db.ForeignKey('learning_entity_behavior.id'))
    uppermost_level_generic_behavior = db.Column(db.BIGINT, db.ForeignKey('learning_entity_behavior.id'))
    behavior_level = db.Column(db.BIGINT)
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))







# 059--学习实体活动--#20211025
class LearningEntitysActivity(BaseModel):
    __tablename__ = 'learning_entitys_activity'
    __table_args__ = (
        UniqueConstraint(
            'activity_starting_entity',
            'activity_ending_behavior',
            name='activity_starting_entity_with_activity_ending_behavior'),)
    learning_entitys_activity = db.Column(db.BIGINT, db.ForeignKey('learning_factor_node.id'), unique=True)
    learning_entitys_activity_name = db.Column(db.String(100), unique=True)
    activity_starting_entity = db.Column(db.BIGINT, db.ForeignKey('learning_entity.id'))
    activity_ending_behavior = db.Column(db.BIGINT, db.ForeignKey('learning_entity_behavior.id'))
    start_time = db.Column(db.DateTime, default=datetime.now)
    end_time = db.Column(db.DateTime)
    is_major = db.Column(db.Boolean, server_default=text('False'))
    description = db.Column(db.String(100))


















# # --概念类型--#20211016
# class ConceptType(BaseModel):
#     __tablename__ = 'concept_type'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     type_name = db.Column(db.String(50), unique=True)
#     classification_method = db.Column(db.BIGINT, db.ForeignKey('classification_method.id'))
#     description = db.Column(db.String(100))
#
#
# # --具体概念项--#20211016
# class SpecificConceptualObject(BaseModel):
#     __tablename__ = 'specific_conceptual_object'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     conceptual_object_text = db.Column(db.String(50), unique=True)
#     description = db.Column(db.String(100))
#
#
#
# # --具体概念类型项--#20211016
# class SpecificTypeConceptualObject(BaseModel):
#     __tablename__ = 'specific_type_conceptual_object'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     conceptual_object_text=db.Column(db.String(100))
#     concept_type= db.Column(db.BIGINT, db.ForeignKey('concept_type.id'))
#     description = db.Column(db.String(100))
#
#
#
# # --具体类型to概念项--多对多--#20211016
# class ConceptualObjectAssociation(BaseModel):
#     __tablename__ = 'conceptual_object_association'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     one_specific_object= db.Column(db.BIGINT, db.ForeignKey('specific_type_conceptual_object.id'))
#     another_specific_object= db.Column(db.BIGINT, db.ForeignKey('specific_type_conceptual_object.id'))
#     description = db.Column(db.String(100))
#
#
# # --数据类型--#20211016
# class DataType(BaseModel):
#     __tablename__ = 'data_type'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     type_name=db.Column(db.String(50),unique=True)
#     text_format=db.Column(db.String(100),unique=True)
#     meaning=db.Column(db.String(100),unique=True)
#     description = db.Column(db.String(100))
#
#
#
# # --模型类型--#20211016
# class ModelType(BaseModel):
#     __tablename__ = 'model_type'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     type_name=db.Column(db.String(50),unique=True)
#     description = db.Column(db.String(100))
#
#
#
# # --教育类型--#20211016
# class EducationalModel(BaseModel):
#     __tablename__ = 'educational_model'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     model_name=db.Column(db.String(50),unique=True)
#     description = db.Column(db.String(100))
#
#
#
#
#
#


# # --xapi相关表--
# # --数据项--
# class DataObject(BaseModel):
#     __tablename__ = 'data_object'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     data_object_name = db.Column(db.String(50), unique=True)
#     data_object_table_name = db.Column(db.String(50), unique=True)
#     description = db.Column(db.String(100))
#
#
# # --主体符号--
# class CustomCharacter(BaseModel):
#     __tablename__ = 'custom_character'
#     id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)
#     specific_character = db.Column(db.String(50), unique=True)
#     text_format = db.Column(db.String(50), unique=True)
#     meaning = db.Column(db.String(50), unique=True)
#     description = db.Column(db.String(100))
#
#
#
# # --学习活动主体--
# # class LearningActivityEntity(BaseModel):
# #     __tablename__ = 'learning_activity_entity'
# #     entity_name = db.Column(db.String(50), unique=True)
# #
# #     entity_full_name = db.Column(db.String(50), unique=True)
# #     starting_ability = db.Column(db.Boolean)
# #     entity_level = db.Column(db.Integer)
# #     upper_level_generic_entity = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity.id'))
# #     uppermost_level_generic_entity = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity.id'))
# #     decomposition_method_applied_in_current_level = db.Column(db.BIGINT, db.ForeignKey('decomposition_method.id'))
# #     keywords = db.Column(db.String(50))
# #     description = db.Column(db.String(100))
#
#
# class LearningActivityEntityProcess(BaseModel):
#     __tablename__ = 'learning_activity_entity_process'
#     entity_process_name = db.Column(db.String(50), unique=True)
#     entity_process_full_name = db.Column(db.String(100), unique=True)
#     affiliated_entity = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity.id'))
#     upper_level_generic_entity_process = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity_process.id'))
#     uppermost_level_generic_entity_process = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity_process.id'))
#     entity_process_level = db.Column(db.Integer)
#     decomposition_method_applied_in_current_level = db.Column(db.BIGINT, db.ForeignKey('decomposition_method.id'))
#     keywords = db.Column(db.String(50))
#     description = db.Column(db.String(100))
#
#
# class LearningActivityType(BaseModel):
#     __tablename__ = 'learning_activity_type'
#     learning_activity_type_name = db.Column(db.String(50), unique=True)
#     learning_activity_type_full_name = db.Column(db.String(50), unique=True)
#     association_starting_entity = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity.id'))
#     association_ending_entity = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity.id'))
#     association_ending_entity_process = db.Column(db.BIGINT, db.ForeignKey('learning_activity_entity_process.id'))
#     keywords = db.Column(db.String(50))
#     description = db.Column(db.String(100))
#
#
# # class TeacherAndStuBehavior(BaseModel):
# #     __tablename__ = 'teacherandstubehavior'
# #
# #     date_time = db.Column(db.DateTime)
# #
# #     # behaviordimension = db.relationship('BehaviorDimension', backref='teacherandstubehavior')
# #
# #     def test_schema(self):
# #         return {
# #             'id': self.id,
# #             'schoolname': self.teacherandstu.schoolinformation.schoolname,
# #             'classname': self.teacherandstu.schoolinformation.classname,
# #             'teacherandstu_id': self.teacherandstu_id,
# #             'teacherandstuname': self.teacherandstu.teacherandstuname,
# #             'behaviordimension_id': self.behaviordimension_id,
# #             'behaviorname': self.behaviordimension.behaviorname,
# #             'date_time': self.date_time,
# #             'teacherandstuidentity': self.teacherandstu.teacherandstuidentity
# #         }
#
#
# # --修改--
# class DefaultClassification(BaseModel):
#     __tablename__ = 'default_classification'
#
#     classification_name = db.Column(db.String(50))
#     another_name = db.Column(db.String(50))
#     classified_description = db.Column(db.String(100))
#
#     def __str__(self):
#         return self.classification_name
#
#
# # --修改--
# class DefaultClassificationDimension(BaseModel):
#     __tablename__ = 'default_classification_dimension'
#     default_classification_id = db.Column(db.Integer)
#     dimension_name = db.Column(db.String(50))
#
#
# # --修改--不确定--
# class BehavioralDataSheet(BaseModel):
#     __tablename__ = 'behavioral_data_sheet'
#     behavioral_data_sheet_name = db.Column(db.String(50))
#
#     def __str__(self):
#         return self.behavioral_data_sheet_name
