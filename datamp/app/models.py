# ORM 类 --》表
# 类对象 --》表中记录
from datetime import datetime
from ext import db
import snowflake.client


# api调用
class BaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.BIGINT, default=snowflake.client.get_guid, primary_key=True)



class MarketSubject(BaseModel):
    __tablename__ = 'market_subject'
    subject_full_name = db.Column(db.String(50))
    subject_type = db.Column(db.BIGINT, db.ForeignKey('market_subject_type.id'))
    subject_unique_code = db.Column(db.String(50), unique=True)
    keywords = db.Column(db.String(50))
    description = db.Column(db.String(100))

    def __str__(self):
        return self.subject_full_name


class MarketSubjectType(BaseModel):
    __tablename__ = 'market_subject_type'
    type_name = db.Column(db.String(50), unique=True)
    keywords = db.Column(db.String(50))
    description = db.Column(db.String(100))


class EducationalApplication(BaseModel):
    __tablename__ = 'educational_application'
    application_name = db.Column(db.String(50))
    application_full_name = db.Column(db.String(50), unique=True)
    version_number = db.Column(db.String(50), unique=True)
    application_owner = db.Column(db.BIGINT, db.ForeignKey('market_subject.id'))
    keywords = db.Column(db.String(50))
    description = db.Column(db.String(100))


class EducationalFunctionModule(BaseModel):
    __tablename__ = 'educational_function_module'
    function_module_name = db.Column(db.String(50))
    affiliated_educational_application = db.Column(db.BIGINT, db.ForeignKey('educational_application.id'))
    function_module_type = db.Column(db.BIGINT, db.ForeignKey('educational_application.id'))
    keywords = db.Column(db.String(50))
    description = db.Column(db.String(100))


class SpecificFunctionalCarrier(BaseModel):
    __tablename__ = 'specific_functional_carrier'
    carrier_name = db.Column(db.String(50))
    carrier_picture = db.Column(db.String(50))
    affiliated_function_module = db.Column(db.BIGINT, db.ForeignKey('educational_function_module.id'))
    keywords = db.Column(db.String(50))
    description = db.Column(db.String(100))


