#创建数据库映射对象
from flask_caching import Cache
from flask_jwt_extended import JWTManager
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_cors import CORS
from flask_wtf import CSRFProtect

db = SQLAlchemy()
bootstrap = Bootstrap()
cache = Cache()
cors= CORS()
api = Api()
jwt = JWTManager()
csrf = CSRFProtect()

