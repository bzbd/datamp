from flask import Flask, make_response, jsonify
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from datamp import create_app
from ext import db


app = create_app()

manager = Manager(app=app)

# 命令工具
migrate = Migrate(app=app, db=db)
manager.add_command('db', MigrateCommand)


@manager.command
def init():
    print('hello')


if __name__ == '__main__':
    print(app.url_map)
    manager.run()
