# 区校两级数据治理系统（后端）

#### 介绍
{**以下是后端说明，您可以替换此简介**
后端采用flask+restful框架，

#### 软件架构
软件架构说明
./app 管理第三方厂商应用
./interface 管理接口规范，接口标准文档管理
./data 管理数据部分，数据处理、分析，模型存储
./user 管理账户等基础信息
ORM框架是SQLAlchemy

第三方拓展有flask_caching、flask_jwt_extended、
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_cors import CORS
from flask_wtf import CSRFProtect


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  开启mysql,pysnowflake
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
