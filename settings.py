import os,os.path
from localsettings import Config

class DevelopmentConfig(Config):
    ENV = 'development'
    # SQLALCHEMY_DATABASE_URI = os.getenv("DB_DEVELOPMENT_URI")
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:12345678@127.0.0.1:3306/flask01'

class TestConfig(Config):
    ENV = 'test'
    DEBUG = False
    # SQLALCHEMY_DATABASE_URI = os.getenv('DB_PRODUCTION_URI')


class ProductionConfig(Config):
    ENV = 'production'
    DEBUG = False
    # SQLALCHEMY_DATABASE_URI = os.getenv('DB_PRODUCTION_URI')



if __name__=='__main__':
    print(Config.BASE_DIR)
    #print(os.path.abspath(__file__))
    print(Config.STATIC_DIR)
    print(Config.UPLOAD_ICON_DIR)



